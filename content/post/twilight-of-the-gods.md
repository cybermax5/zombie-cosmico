---
author: Musgo de Buey
date: 2021-04-27
title: Twilight of the Gods - Bathory
image: /musica/twilight-of-the-gods.jpg
tags: [
    "Música"
]
---

Corría el '91, la gente andaba flipando con el grunge de Kurt Cobain y todavía no había pasado aquello de quemar iglesias por el norte de Europa (pero todo se andaría)... **Quorthon**, el mesías del Viking Metal, se la sacaba una vez más y nos invitaba a un viaje onírico y mágico, épico y seminal... un viaje conocido como "Twilight of the Gods" (El Ocaso de los Dioses). La portada, al estilo de Bathory, ya es una declaración de intenciones.

El título homónimo del disco, *"Twilight of the Gods"*, arranca acústico y descarnado y va ganado densidad, peso, forma... hasta que se convierte en un viaje mágico e iniciático de 11 minutazos. *"Through Blood by Thunder"* profundiza en las líneas de la anterior y con su medio tiempo prepara el terreno para *"Blood and Iron"*, uno de los principales temazos del álbum. Punteo inicial preciosista seguido de una intensidad difícilmente transmitible. Y sin abusar del ruido clásico del metal extremo, ojo. Intensidad y pasión autocontenidas, moldeadas y destiladas hasta lograr su pureza máxima.

*"Under The Runes"*, *"To Enter your Mountain"* y *"Bond of Blood"*, son tres temazos más que se suman a lo mejor de la banda y dan cuerpo al disco. Serían las típicas pistas que en otro disco brillarían con luz propia, pero que aquí se eclipsan unas a otras.

A este disco se le ha acusado de ser más light que los anteriores y bueno, es verdad que es más ligero, pero eso no quiere decir que sea peor ni muchísimo menos. Es más asequible en tanto que la agresividad no se deja ver bien en cada riff y en cada blast, pero ahí está. Digamos que es una violencia flemática.

Y ahora llega "Hammerheart", el tema que cierra el disco y deja lo anterior a la altura del grunge. Si tuviera que elegir una única composición de Bathory no dudaría ni un segundo que sería este *"Hammerheart"*. El tema se articula alrededor de la melodía de **Saturno** de **Gustav Holst** y resulta más solemne que épico, más profundo que intenso, más melancólico que rabioso. Genera una atmósfera de recogimiento, de descanso, de retorno al hogar que no se puede expresar con palabras. *"Hammerheart"* es eso y mucho más, pues fue el broche de oro de la despedida a Quorthon, su fin del camino. Lo único que podemos lamentar del tema es su corta duración.