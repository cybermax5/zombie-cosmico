---
author: Musgo de Buey
date: 2021-05-01
title: La Orden de los Nueve Ángulos
image: /curiosidades/orden-9-angulos.jpg
tags: [
    "Curiosidades"
]
---
El misterio rodea la **Orden de los Nueve Ángulos**. Aunque hay mucha información ahí afuera, hay quien dice que en realidad es una secta sin miembros, que la verdadera Orden es súper secreta, que no se puede saber si tiene miembros o no y hasta quien dice que fue un experimento fallido neonazi como herramienta de captación y que nunca existió realmente.

No hay que confundir la *Orden de los Nueve Ángulos* u Order of the Nine Angles (abreviaba a ONA, o también O9A) con la *Orden de los Nueve Ángeles* u Order of the Nine Angels, organización de místicos de los años 20 y 30 que organizaban expediciones a África para encontrar restos de atlantes.

Esta secta satánica aparece en Gran Bretaña en la década de 1960 tras la unión de los templos neopaganos llamados *Camlad*, *The Noctulians* y *Temple of the Sun*. Tras la emigración del líder a Australia, diversas fuentes afirman que David Myatt asumió el control de la orden y empezó a escribir las enseñanzas ahora públicas de la Orden de los Nueve Ángulos, bajo el seudónimo de Christos Beest. En 2002 habían grupos en EEUU, Europa, Australia, Nueva Zelanda, Canadá y Rusia.

La Orden define el satanismo como una búsqueda fuertemente individual, que ha de crear sabiduría y excelencia mediante retos que permitan trascender los límites físicos y mentales, con una actitud “pagana, fiera y libre” ante la vida. Esto, claro, habría de llevar a una sociedad superior.

No obstante, no puede relacionarse la Orden de los Nueve Ángulos con las formas más típicas de satanismo, incluso cuando estas se acercan a filosofías supremacistas. La propia ONA deja claro que ni tiene ni quiere conexión alguna con la organización más numerosa, la Iglesia de Satán, a la que desprecia: afirma que la “Biblia Satánica” de Anton LaVey es una “filosofía aguada”. También rechaza la perspectiva del Templo de Set por considerarla “religiosa”; esta organización, la segunda más importante en el satanismo, también se encuentra opuesta a la Orden debido a su defensa del sacrificio humano, aunque reconocen que es su mayor diferencia de opinión.

La Orden de los Nueve Ángulos daría para rellenar unos cuantos reality-shows sobre organizaciones satánicas si uno hubiera de confiar en sus escritos. Los textos de la organización justifican y animan al sacrificio humano como método para la eliminación de los débiles: “una contribución para mejorar al ser humano, acabando con los que carecen de valor, los débiles, los enfermos (en términos de carácter)”. Así, afirman que el verdadero satanismo necesita que uno se aventure en lo prohibido e ilegal para contactar con la “esfera de lo acausal”, las “fuerzas siniestras del cosmos”. Presenciar tales energías acausales habrá de crear un nuevo Aeon cuyas energías darán lugar a una nueva civilización superior.

De hecho, el objetivo que la Orden de los Nueve Ángulos define para sí, es desafiar la noción aceptada de “satanismo” representada por grupos como el Templo de Set y la Iglesia de Satán de LaVey. Así, distinguirían especialmente entre el *satanismo aceptado* del grupo americano del Templo de Set liderado por el Doctor Aquino, con el *satanismo tradicional* que representaría su orden. Su labor sería revelar poco a poco "la verdad" de este satanismo tradicional, mediante cartas públicas y el magazine underground “Fenrir”, hasta que organizaciones como el propio Templo de Set hicieron un rechazo público del grupo, en particular por el ya mencionado apoyo a la idea del sacrificio humano del débil. 