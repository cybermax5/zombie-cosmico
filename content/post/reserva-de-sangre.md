---
author: Musgo de Buey
date: 2021-04-29
title: Modificar la Reserva de Sangre en Vampiro la Mascarada
image: /rol/vampiro.jpg
tags: [
    "Rol"
]
---

Llevo muchos, muchos años dirigiendo a Vampiro la Mascarada y durante todo este tiempo en cada crónica que juego cambio algo. Sabido es que no me gusta nada la metatrama y aunque normalmente utilizo muy poco el Sabbat cuando no ignoro completamente la Yihad, no han faltado ocasiones en las que he eliminado los clanes directamente.

Sin embargo, hace unos años hice una modificación más drástica que estaba relacionada con uno de los elementos centrales del sistema de juego, la Reserva de Sangre. He aquí las reglas caseras que hice y los efectos que tuvieron, por si a aquellos que se mantienen con las reglas de las ediciones anteriores a V5 le sirve para algo.

Los cambios son dos: doblar la Reserva de Sangre y reducir la Vitae que proporcionan los humanos. La Reserva de Sangre se multiplica por dos sin más miramientos. Esto hace que los vampiros, cualquiera de ellos, tengan mucho más poder potencial, que es lo que persigo con esta regla. Sé que los cainitas son bastante poderosos de por sí, pero no creo que sean lo suficiente al compararlo con humanos. Para mí es importante, para mantener el arquetipo clásico del vampiro, que un neonato abrazado hace unas pocas noches de un clan con conceptos débiles en lo físico (un tremere, por ejemplo) sea capaz de partir a la mitad a un mortal y que ello no sea una proeza que le deje famélico.

Es cierto que al aplicarse esta multiplicación de la Reserva de Sangre de forma lineal a todos los vampiros, los más antiguos pierden poder en relación a los más jóvenes en tanto que antes los de generación más baja sí podían hacer lo que yo creo que debería ser patrimonio de todos. Aún así creo que es un precio pequeño a pagar, porque llegado el caso, un octava sigue teniendo un 50% más de vitae que un neonato de decimotercera.

Ahora bien, en realidad y a pesar del incremento de poder en término absolutos, yo lo que busco es la segunda modificación, el limitar de forma importante la vitae que proporciona un humano. Doblar la Reserva de Sangre es solo para dar un poco de margen a los jugadores que no son conscientes de lo que les va a costar alimentarse.

En mi nueva escala de obtención de sangre de un mortal la cosa queda más o menos así:

- Perder un punto deja a la presa mareada, desorientada y al borde del bajón de tensión.
- Perder dos puntos la deja en estado de shock circulatorio, lo que probablemente le cause la muerte si se les abandona sin más (con una muy posible pérdida de Humanidad y consecuente ruptura de la Mascarada).
- Perder tres puntos mata a la víctima (con una muy posible pérdida de Humanidad y consecuente ruptura de la Mascarada).


La puesta en juego de esta segunda regla hace muy difícil alimentarse, teniendo que recurrir a grandes cantidades de recipientes o hacer una verdadera carnicería, reforzando así la imagen clásica del vampiro sediento y poderoso a ojos de los mortales (al margen de otras sutiles diferencias entre clanes).

Una vez puesta en práctica esta combinación de reglas caseras genera vampiros mucho más poderosos, pero complica la alimentación, con lo que si bien los PJs podrán actuar sin muchos miramientos cuando la situación lo requiera, van a mirar mucho cada punto de vitae que gastan al ser conscientes de lo duro que es reponerse. El jugador acostumbrado a abusar de la Celeridad rápidamente se verá condicionado por su hambre permanente.

Pongamos que una neonata se enfrenta a unos cazadores novatos, que tras usar escopetas, estacas, ajo, cruces y agua bendita, le terminan impactando un cóctel molotov. La vampira, envuelta en una bola de fuego, con la espalda y una pierna trituradas por escopetazos y dos estacas a medio clavar en el pecho, con sus atributos hinchados por la vitae al máximo, es capaz de curarse del daño no agravado y terminar con las manos desnudas con los mortales para luego arrastrarse a su cubil con 5 niveles de daño agravado por el fuego.

Hagamos una cálculo mental rápido sobre esto último: la vampira con 5 agravados necesita 25 puntos de vitae y 5 semanas para recuperarse (lo que implica otros 35 puntos de vitae para mantenerse despierto cada noche de esas 5 semanas). Con este sistema y sin matar a nadie para alimentarse necesitaría entre 30 y 60 sesiones de caza, algo realmente complicado estando tan herido, al menos inicialmente. Si no dispone de ayuda externa (Criados, Aliados, Rebaño) tardará bastantes meses en recuperarse.

Por supuesto también da lugar a que un vampiro pueda desatender su alimentación durante semanas para de vez en cuando darse un auténtico festín alimentándose de decenas de presas, pero también a que otro gravemente herido tarde semanas cuando no meses en recuperarse del todo, sobre todo si no quiere dejar un reguero de cadáveres secos.

Un antiguo ventrue, el clásico depredador inhumano, podría estar tranquilamente 2 semanas sin alimentarse y aún así no tener hambre, mientras espera a que sus lacayos hayan encontrado el número suficiente de víctimas adecuadas para darse un festín.

Soy consciente de que el sistema nuevo de V5 está realmente bien y probablemente encaja mejor con el tono de Vampiro en general que el viejo sistema de Reserva de Sangre (con o sin modificaciones). Además tiene la importante pega de que el proceso de alimentación se vuelva tan tedioso que pase a hacerse fuera de juego, eliminando así la carga dramática que se le supone. Puede ser, no lo niego y de hecho actualmente no uso este sistema, sin embargo lo hice hace años con muy buenos resultados para el tipo de crónica que jugábamos entonces: intriga política con algún combate ocasional. Y ya os digo que aquí eran los jugadores los que trataban de evitar situaciones conflictivas y no malgastar cada fracción de su preciada vitae.