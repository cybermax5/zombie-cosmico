---
author: Musgo de Buey
date: 2021-07-26
title: A blaze in the northern sky - Darkthrone
image: /musica/a-blaze-in-the-northern-sky.jpg
tags: [
    "Música"
]
---

Este disco es especial porque fue el primer álbum de metal extremo con el que me hice. Antes me había comprado **"Into the Pandemonium"** de **Celtic Frost**, pero en aquella compra me guiaba únicamente por la portada sacada de El jardín de las delicias de El Bosco y tampoco es que sea un trabajo muy extremo. En los albores del siglo XXI, el black metal era para mí como el grunge para mis amigos.

Sea como sea **"A Blaze in the Northern Sky"** llegó con su rollo true black metal, a una mente deseosa de meterse de lleno en todo aquello. Mientras la gente acudía a los cibers para chatear con desconocidos y descargarse fotos guarras, yo buscaba información sobre el *Inner Circle* (también descargaba fotos guarras, claro).

Para ponernos en antecedentes, Darkthrone forma parte de lo que vino a llamarse la segunda ola del black metal que se dio fundamentalmente en Noruega a finales de los 80 y principios de los 90, y que está parcialmente relacionado con una agenda anti-cristiana (quema de iglesias incluída) y algunas muertes. En cierto modo esta segunda ola del black metal es en realidad la primera que cuenta con bandas y un estilo plenamente asentados.

El anterior (y primer) trabajo de Darkthrone, **"Soulside jorney"** es más deathmetalero que otra cosa, pero en este decidieron dar un paso adelante y pasarse al lado más true y desquiciado de este cotarro extremo, lo que costó algunos disgustos en la discrográfica y supuso la marcha del bajista Dag Nilsen. Seducidos por Euronymous (Mayhem) se cargaron de pinchos, se pintarrajearon de blanco y sacaron este disco que coincide con el epicentro del famoso asesinato de Euronymous a manos de Vikernes (Burzum). Total, que al calor de la sangre y el fuego (de las iglesias que quemaban), este trabajo y los siguientes situaron a Darkthrone como una de las principales puntas de lanza del movimiento.

A día de hoy sigo recordando la primera sensación que tuve al escuchar **"Kathaarian Life Code"**, con su intro lejana y fantasmal, con sus afilados riffs, con su agresividad primitiva y tosca. Amor a primera vista, vaya. Una sensación que no volví a sentir hasta llegar a Nile. Es un tema absorbente e hipnotizante que funciona como un buen resumen del disco en sí.

Una vez pasado este primer corte iniciático nos encontramos con **"In the Shadow of the Horns"**, un tema duro, con esa voz áspera pero no gutural de Nocturno Culto y unos riffs distorsionados y machacones que pasan a una velocidad que no es ni rápida ni lenta, es lo que tiene que ser: densa y oscura.

**"Paragon Belial"** es un medio tiempo con una guitarra relativamente poco distorsionada que incluso puede hacer las veces de pausa relajante en el disco, si bien batería, voz y la no-producción redondean el tema y le dan el aire adecuado.

Si "Parangon Belial" podía servir de descanso, queda claro que **"Where Cold Winds Blow"** es todo lo contrario. Arranca con una veloz batería y de ahí ya no baja hasta un breve interludio a mitad de tema, todo acompañado de una distorsión muy sucia y adecuada. Suena a black metal puro y duro. Además tiene ese título tan lovecraftiano...

El tema homónimo del disco, **"A Blaze in the Northern Sky"**, es algo a medio camino entre "Parangon..." y "Where...", con bastante alternancia de ritmo, cambios de distorsión y solos que por sí solos sonarían a heavy clásico. Es la canción más corta del disco, pero puede que la más variada.

Y cerramos con la increíble **"Pagan Winter"** como colofón y conclusión a este viaje de poco más de 40 minutos. Si "Kathaarian..." implicaba adentrarse en las tinieblas de lo desconocido, "Pagan Winter" es emerger al mundo real, también es una advertencia, un "lo que pasa con Darkthrone, se queda con Darkthrone". No es un adiós, es un hasta luego, porque es muy posible que después de esto quieras repetir... Aunque puede que no inmediatamente, que para el neófito puede ser agotador.

**A Blaze in the Northern Sky** no sólo es un clásico del Black Metal de pleno derecho, sino que es un buen disco para iniciarse en todo este rollo. No es muy largo, la voz no es tan extrema, no usa blast beats ni es excepcionalmente agresivo y además hay un par de temas con pasajes atmosféricos fáciles de escuchar.