---
author: Musgo de Buey
date: 2021-04-24
title: Ya no surfeamos la web
image: computer.jpg
tags: [
    "Reflexiones"
]
---

Hace poco tuve un ataque de nostalgia por aquello de "surfear" en la web. Se combinaron un post sobre PCs antiguos en un foro y un toot en mastodon sobre el hosting para webs estáticas neocities.org (una clara alusión a geocities, donde muchos subíamos nuestras webs). Estando así, volví a escuchar el capítulo de [Reality Cracking sobre la web de los 90](https://archive.org/details/realitycracking/fl+cosas-echo-menos-internet.mp3) y es cierto que el actual internet (en realidad el www) está basado en webs comerciales, redes sociales, foros y plataformas de blogs, todo ello no solo sin mucha interconexión con el resto, sino dependiendo de unas pocas multinacionales.

En los 90 había muchísimas más webs personales en proporción y todas tenían una nutrida sección de links que permitían saltar de unos sitios a otros sin saber muy bien donde ibas a terminar. Había intercambio de banners y anillos web desde donde directamente podías saltar aleatoriamente a otras webs relacionadas de algún modo con la que web en la que estabas.

Además esas webs estaban hospedadas en miles de servidores propios o alquilados a empresas  de hosting, normalmente a cambio de publicidad. El hosting era limitado tanto en espacio en disco duro como en ancho de banda, los subdominos eran horribles y muchas de estas empresas tendían a desaparecer, así que acostumbrabas a tener copias de seguridad de tu web. No sólo creabas el contenido en tu máquina local, sino que lo subías, puntual y manualmente, a través de FTP.

En aquellos tiempos una web personal podía recibir del orden de unos cientos de visitas al mes, la mayoría del tráfico era casual, y el ritmo de publicación de contenido también era muchísimo más lento que el actual. Sin ir más lejos, era la época de los libros de visitas (una sección donde los visitantes podían dejar un comentario a modo de firma), porque era una de las pocas formas de interacción directa con entre visitantes y webmasters.

Con el salto a los blogs pasaron dos cosas: que ya no hacía falta conocimientos técnicos para montar una web y que la generación de contenido se automatizó muchísimo. Ahora cualquier podía tener su web, lo cual estaba muy bien, pero la enorme facilidad y rapidez de generación de contenido también hizo, creo yo, que apareciese un marasmo de contenidos irrelevantes y caducos que a la postre terminaron por inundar la web.

Es normal que pasase eso, blog es la abreviación de "web log" (algo así como "diario web"), pero a esto hay que añadir que la interconexión inicial de blogs a través de las plataformas que los sostenía multiplicó el tráfico a la vez que reforzaba el ego del bloguero a través de los comentarios.

Mantener un blog era tan sencillo... te dabas de alta en la plataforma, escogías una plantilla y ya podías empezar a publicar cosas.

En poco tiempo, la facilidad de uso de los blogs y su mayor *engagement* desplazaron completamente las webs personales. Lamentablemente cada vez más blogueros no escribían tanto con el afán de compartir contenidos como con el del ser el centro de atención y esta no es una afirmación tan gratuita como pueda parecer: en cuanto las redes sociales despuntaron, buena parte de los blogueros pretendieron mutar en los influencers de su minúscula área de influencia.

Y así estamos hoy, en la que las webs personales son dinosaurios extintos y los blogs están en decadencia gracias al auge de las redes sociales. Si las plataformas de blogs generaban más visitas, comentarios y tráfico recurrente con la aparición de los primeros seguidores que las viejas webs, las redes sociales son el siguiente salto de magnitud en impacto respecto a los blogs.

De hecho en el punto en el que estamos ya no importa tanto la plataforma desde donde se publican contenidos como el usuario, el perfil, la "marca personal" con la que se identifica al autor (el dominio, la infraestructura donde se alojan los contenidos, el tráfico y los metadatos resultantes pertenecen a la multinacional que hace negocio con todo esto, por cierto).

¿Y a donde nos lleva esto? A que todo este proceso no es ni más ni menos que "concentración de capital", asumiendo que en este caso capital es tanto el capital humano detrás de la creación de contenido, como el capital tecnológico que sustenta las plataformas donde se publica.

En la medida que hemos dejado de *surfear*, también hemos dejado que el oleaje se pare y ahora, tristemente, solo quedan piscinas vigiladas por socorristas profesionales.