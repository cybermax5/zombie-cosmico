---
author: Musgo de Buey
date: 2021-04-28
title: Fanzines sobre metal extremo
image: /musica/mayhem.jpg
tags: [
    "Música"
]
---

Buscando material para preparar una partida de rol me he encontrado con esta web llena de viejos fanzines sobre metal extremo listos para ser descargados. Están ordenador alfabéticamente, incluyen país e idioma y son unas cuentas decenas.

Algunos tienen una calidad bastante lamentable, pero es lo que tenían los fanzines del siglo XX: una maquetación extremadamente artesanal y una calidad ínfima de reproducción (normalmente fotocopias de fotocopias).

[Send back my stamps](https://sendbackmystamps.org/fanzine-pdf-downloads/)

Una mención especial a [Drowned Magazine nº 2](https://sendbackmystamps.files.wordpress.com/2015/05/drowned-mag-2.pdf), escrito en su totalidad por **Dave Roten**, el cantante de la banda de death metal Avulsed, que me ha hecho especial ilusión encontrar. 