---
author: Musgo de Buey
date: 2021-04-30
title: Custer
image: /comic/custer.jpg
tags: [
    "Cómic",
    "Cyberpunk"
]
---

Guionizado por el argentino **Carlos Trillo** (*Alvar Mayor*, *Clara de noche*) y dibujado por el español **Jordi Bernet** (*Torpedo 1936*, *Kraken*, *Clara de noche*, *Andrax*), cuenta una serie de historias cortas sobre una mujer que es seguida al segundo por cámaras de la CBN, una cadena televisiva que realiza un programa basado en su vida (todos los jueves a las 10 de la noche). Sí, ya sé, es el argumento del Show de Truman, sólo que Custer se escribió unos 15 años antes.

A través de cortas historias (de 3 o 4 páginas) vamos viendo los pensamientos de Custer, el hastío que le produce su modo de vida y los recuerdos de una vida feliz a la que no puede volver. Mientras sucede todo esto, somos testigos de la degradación del entorno, el absurdo de una realidad distópica y ultraviolenta de la que los autores protegen a la protagonistas utilizando sus pensamientos como burbuja protectora y humanizante. Además de Custer, los otros dos personajes recurrentes no son personas al uso: una voz grabada en un casete y la voz en off de la producción de la cadena de televisión.

De hecho, los tres puntales principales sobre los que descansan las historias de Custer son la relación de ésta con la voz del casete y su vínculo con su vida pasada, el poder de la voz en off que hace y deshace a su antojo la historia que está contando y la deshumanización del mundo, en clave de humor negro: ¡hamburguesas de novio muerto!.

Respecto al arte, **Jordi Bernet** tiene un estilo muy marcado y quien haya leído *Clara de noche* en El Jueves ya tiene un buen ejemplo de como dibuja. Alternando entre unos personajes realistas y otros más caricaturescos, mezcla las gabardinas y los sombreros fedora con montones de punks ochenteros, todos ellos en escenarios sórdidos a contraluz, callejones oscuros y camas que ofrecen sexo.

![Custer, de Trillo y Bernet](/images/comic/custer-1.jpg)
![Custer, de Trillo y Bernet](/images/comic/custer-2.jpg)